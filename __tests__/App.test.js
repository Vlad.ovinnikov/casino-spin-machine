/** Created by: Vladyslav
 * Date: 23/01/2019
 * Time: 15:55
 */
import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { configure, shallow } from 'enzyme';
import App from '../src/App';
import { Main } from '../src/layouts';

configure({ adapter: new Adapter() });

it('App component working', () => {
    const wrapper = shallow(<App/>);

    expect(wrapper.find(Main).length).toEqual(1);
});
