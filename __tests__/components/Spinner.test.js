/** Created by: Vladyslav
 * Date: 23/01/2019
 * Time: 15:55
 */
import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { configure, shallow } from 'enzyme';
import { Spinner } from '../../src/components';

configure({ adapter: new Adapter() });
const items = ['strawberry', 'banana', 'orange', 'a monkey'];

it('Spinner component working', () => {
    const onFinish = jest.fn(),
        spinner = shallow(
            <Spinner
                items={ items }
                timer={ 1000 }
                isStart={ false }
                onFinish={ onFinish }/>);

    expect(spinner.instance().props.items).toEqual(items);
    expect(spinner.instance().props.timer).toEqual(1000);
    expect(spinner.instance().props.isStart).toBeFalsy();
    expect(spinner.instance().state.position).toEqual(0);
    expect(spinner.instance().state.timeRemaining).toBeNull();
    expect(spinner.instance().state).not.toBeNull();
});

it('Start spinning', () => {
    const onFinish = jest.fn(),
        spinner = shallow(
            <Spinner
                items={ items }
                timer={ 1000 }
                isStart={ true }
                onFinish={ onFinish }/>);

    spinner.instance().updateHandler();
    expect(spinner.instance().timer).not.toBeUndefined();
    expect(spinner.instance().state.timeRemaining).toEqual(spinner.instance().props.timer);
    expect(spinner.instance().state.position).toEqual(spinner.instance().start);
});

it('changePosition should change position and add 100ms to the timeRemaining', () => {
    const onFinish = jest.fn(),
        spinner = shallow(
            <Spinner
                items={ items }
                timer={ 1000 }
                isStart={ true }
                onFinish={ onFinish }/>);

    spinner.instance().changePosition();
    expect(spinner.instance().state.position).not.toBeNull();
    expect(spinner.instance().state.timeRemaining).toEqual(100);

});

it('stopHandler should clear timer and run action onFinish', () => {
    const onFinish = jest.fn(),
        spinner = shallow(<Spinner
            items={ items }
            timer={ 1000 }
            isStart={ true }
            onFinish={ onFinish }/>);

    spinner.instance().timer = 1;
    spinner.instance().stopHandler();

    expect(onFinish).toHaveBeenCalledTimes(1);
});

it('tick run action onFinish when timeRemaining more than 9800', () => {
    const onFinish = jest.fn(),
        spinner = shallow(
            <Spinner
                items={ items }
                timer={ 1000 }
                isStart={ true }
                onFinish={ onFinish }/>);

    spinner.setState({ timeRemaining: 9800 });
    spinner.instance().changePosition();
    spinner.instance().changePosition();
    spinner.instance().tick();
    expect(spinner.instance().state.timeRemaining).toEqual(10000);
    expect(onFinish).toHaveBeenCalledTimes(1);
});

it('tick run changePosition when timeRemaining less than 9800', () => {
    const onFinish = jest.fn(),
        spinner = shallow(
            <Spinner
                items={ items }
                timer={ 1000 }
                isStart={ true }
                onFinish={ onFinish }/>),
        spyChangePosition = jest.spyOn(spinner.instance(), 'changePosition');

    spinner.setState({ timeRemaining: 1000 });
    spinner.instance().tick();

    expect(spyChangePosition).toHaveBeenCalledTimes(1);
});

it('updateHandler function should clear timer if exists', () => {
    const onFinish = jest.fn(),
        spinner = shallow(
            <Spinner
                items={ items }
                timer={ 1000 }
                isStart={ true }
                onFinish={ onFinish }/>);

    jest.useFakeTimers();

    spinner.instance().timer = 1;
    spinner.instance().updateHandler();

    expect(clearInterval).toHaveBeenCalledWith(expect.any(Number));
});

it('updateHandler function should run tick function with setInterval', () => {
    const onFinish = jest.fn(),
        spinner = shallow(
            <Spinner
                items={ items }
                timer={ 1000 }
                isStart={ true }
                onFinish={ onFinish }/>),
        spyTick = jest.spyOn(spinner.instance(), 'tick');

    jest.useFakeTimers();

    expect(spyTick).not.toBeCalled();

    spinner.instance().timer = null;
    spinner.instance().updateHandler();

    jest.runAllTimers();

    expect(setInterval).toHaveBeenCalledWith(expect.any(Function), 50);
    expect(spyTick).toHaveBeenCalled();
    expect(spyTick).toHaveBeenCalledTimes(89);
});

it('updateHandler function should run in componentDidUpdate when this.props.isStart = true', () => {
    const onFinish = jest.fn(),
        spinner = shallow(
            <Spinner
                items={ items }
                timer={ 1000 }
                isStart={ false }
                onFinish={ onFinish }/>),
        spyUpdateHandler = jest.spyOn(spinner.instance(), 'updateHandler');

    spinner.setProps({ isStart: true });
    spinner.update();

    expect(spyUpdateHandler).toHaveBeenCalledTimes(1);

});

it('stopHandler function should run in componentDidUpdate when this.props.isStart = false', () => {
    const onFinish = jest.fn(),
        spinner = shallow(
            <Spinner
                items={ items }
                timer={ 1000 }
                isStart={ true }
                onFinish={ onFinish }/>),
        spyStopHandler = jest.spyOn(spinner.instance(), 'stopHandler');

    spinner.setProps({ isStart: false });
    spinner.update();

    expect(spyStopHandler).toHaveBeenCalledTimes(1);

});
