/** Created by: Vladyslav
 * Date: 23/01/2019
 * Time: 15:55
 */
import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { configure, shallow } from 'enzyme';
import { Main } from '../../src/layouts';

configure({ adapter: new Adapter() });

it('Main component working', () => {
    const main = shallow(<Main/>);

    expect(main.instance().state.winner).toBeNull();
    expect(main.instance().state.start).toBeFalsy();
    expect(main.instance().matches.length).toEqual(0);
    expect(main.instance().items.length).toEqual(4);
    expect(main.instance().items[0]).toEqual('strawberry');
    expect(main.instance().spinners.length).toEqual(3);
    expect(main.instance().spinners[0]).toEqual(2200);
    expect(main.instance().timer).toEqual(2);
});

it('startHandle function should start spinning', () => {
    const main = shallow(<Main/>);

    main.instance().startHandle();
    expect(main.instance().state.start).toBeTruthy();
});

it('stopHandle function should stop spinning', () => {
    const main = shallow(<Main/>);

    main.instance().stopHandle();
    expect(main.instance().state.start).toBeFalsy();
});

it('checkPrize Function Should Return Correct Values', () => {
    const main = shallow(<Main/>);

    main.instance().finishHandler('a monkey');
    main.instance().finishHandler('a monkey');
    main.instance().finishHandler('a monkey');
    expect(main.instance().state.winner).toEqual({
        dupl: { consecutive: true, el: 'a monkey' },
        full: true
    });

    main.instance().finishHandler('a monkey');
    main.instance().finishHandler('a monkey');
    main.instance().finishHandler('banana');
    expect(main.instance().state.winner).toEqual({
        dupl: { consecutive: true, el: 'a monkey' },
        full: false
    });

    main.instance().finishHandler('a monkey');
    main.instance().finishHandler('banana');
    main.instance().finishHandler('a monkey');
    expect(main.instance().state.winner).toEqual({
        dupl: { consecutive: false, el: 'a monkey' },
        full: false
    });

    main.instance().finishHandler('orange');
    main.instance().finishHandler('banana');
    main.instance().finishHandler('a monkey');
    expect(main.instance().state.winner).toEqual({
        dupl: {},
        full: false
    });
});

it('setTimeout function should start timeout then run callback function', () => {
    const main = shallow(<Main/>),
        cbFunc = jest.fn();

    jest.useFakeTimers();

    expect(cbFunc).not.toBeCalled();

    main.instance().setTimeout(cbFunc, 1000);

    jest.runAllTimers();

    expect(setTimeout).toHaveBeenCalledWith(expect.any(Function), 1000);
    expect(main.instance().timer).not.toBeNull();
    expect(cbFunc).toHaveBeenCalled();
    expect(cbFunc).toHaveBeenCalledTimes(1);
});

it('clearTimeout function should called on componentWillUnmount', () => {
    const main = shallow(<Main/>);
    const spyComponentWillUnmount = jest.spyOn(main.instance(), 'componentWillUnmount');

    main.unmount();

    expect(spyComponentWillUnmount).toHaveBeenCalled();
});
