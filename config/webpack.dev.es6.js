/**
 * Created by: Vladyslav
 * Date: 19/01/2019
 * Time: 11:32
 */
import path from 'path';
import webpack from 'webpack';
import autoPrefixer from 'autoprefixer';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import HtmlWebPackPlugin from 'html-webpack-plugin';

const { HOST, PORT } = process.env,
    { WatchIgnorePlugin } = webpack,
    config = {
        devtool: 'source-map',
        mode: 'development',
        entry: [
            '@babel/polyfill',
            path.resolve(__dirname, '../src')
        ],
        output: {
            path: path.resolve('dist', '../assets/'),
            publicPath: '/',
            filename: '[name].[hash].js',
            chunkFilename: '[name].[chunkhash].js'
        },
        optimization: {
            splitChunks: {
                chunks: 'all',
                minSize: 10000,
                minChunks: 1,
                maxAsyncRequests: 5,
                maxInitialRequests: 3,
                automaticNameDelimiter: '~',
                name: true,
                cacheGroups: {
                    vendors: {
                        test: /[\\/]node_modules[\\/]/,
                        priority: -10
                    },
                    default: {
                        minChunks: 1,
                        priority: -20,
                        reuseExistingChunk: true
                    }
                }
            }
        },
        stats: {
            // Add timing information
            timings: false,
            // Add webpack version information
            version: false,
            // Add warnings
            warnings: true,
            // Add errors
            errors: true,
            // Add details to errors (like resolving log)
            errorDetails: true,
            // Add built modules information
            modules: true,
            // Show performance hint when file size exceeds `performance.maxAssetSize`
            performance: true,
            // Show dependencies and origin of warnings/errors (since webpack 2.5.0)
            moduleTrace: true,
            // Add information about the reasons why modules are included
            reasons: true
        },
        devServer: {
            stats: 'errors-only', // Display only errors to reduce the amount of output
            host: HOST, // Defaults to `localhost`
            port: PORT, // Defaults to 8080
            open: true, // Open the page in browser
            overlay: true,
            historyApiFallback: true,
            watchOptions: {
                // Delay the rebuild after the first change
                aggregateTimeout: 300,
                // Poll using interval (in ms, accepts boolean too)
                poll: 1000
            }
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    enforce: 'pre',
                    use: [
                        {
                            options: {
                                eslintPath: 'eslint'
                            },
                            loader: 'eslint-loader'
                        }
                    ]
                },
                {
                    test: /\.(js|jsx)$/,
                    use: {
                        loader: 'babel-loader'
                    },
                    exclude: [
                        /node_modules/
                    ]
                },
                {
                    test: /\.scss$/,
                    use: ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: [
                            {
                                loader: 'css-loader',
                                options: {
                                    url: false,
                                    importLoaders: 1,
                                    modules: true,
                                    localIdentName: '[name]__[local]__[hash:base64:5]'
                                }
                            },
                            {
                                loader: 'sass-loader',
                                options: {
                                    sourceMap: true
                                }
                            }
                        ]
                    })
                },
                {
                    test: /\.css$/,
                    use: ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: [
                            {
                                loader: 'css-loader',
                                options: {
                                    url: false,
                                    importLoaders: 1,
                                    modules: true,
                                    localIdentName: '[name]__[local]__[hash:base64:5]'
                                }
                            },
                            'postcss-loader'
                        ]
                    })
                },
                {
                    test: /\.(jpe?g|png|gif|svg)$/i,
                    loader: 'url-loader?name=images/[name].[ext]'
                }
            ]
        },
        resolve: {
            extensions: ['.jsx', '.js', '.css', '.scss']
        },
        plugins: [
            new HtmlWebPackPlugin({
                title: 'Casino spin machine',
                template: `${ __dirname }/../public/index.html`
            }),
            // Ignore node_modules so CPU usage with poll
            // watching drops significantly.
            new WatchIgnorePlugin([
                path.join(__dirname, 'node_modules'),
                path.join(__dirname, '__tests__')
            ]),
            new ExtractTextPlugin({
                filename: 'styles.css',
                allChunks: true
            }),
            autoPrefixer
        ]
    };

export default config;
