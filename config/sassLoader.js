/**
 * Created by: Vladyslav
 * Date: 19/01/2019
 * Time: 11:34
 */

const sass = require('node-sass');

module.exports = (data, file) => {

    try {
        return sass.renderSync({ data, file }).css.toString('utf8');
    } catch (e) {
        console.error(e);
    }

};
