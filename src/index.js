/**
 * Created by: Vladyslav
 * Date: 19/01/2019
 * Time: 11:33
 */
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

ReactDOM.render(
    <App/>, document.getElementById('root')
);
