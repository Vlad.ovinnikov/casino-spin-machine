/**
 * Created by: Jax
 * Date: 2019-01-22
 * Time: 17:11
 */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

class Spinner extends PureComponent {

    static propTypes = {
        items: PropTypes.array.isRequired,
        onFinish: PropTypes.func.isRequired,
        timer: PropTypes.number,
        isStart: PropTypes.bool
    };

    state = {
        position: 0,
        timeRemaining: null
    };

    start = Spinner.SetPosition();

    constructor(props) {
        super(props);

        this.updateHandler = this.updateHandler.bind(this);
        this.stopHandler = this.stopHandler.bind(this);
    }

    componentDidUpdate(prevProps) {
        if (this.props.isStart && prevProps.isStart !== this.props.isStart) {
            this.updateHandler();

        } else if (!this.props.isStart && prevProps.isStart !== this.props.isStart) {

            this.stopHandler();

        }
    }

    updateHandler() {
        if (this.timer) {
            clearInterval(this.timer);
        }

        this.setState({
            position: this.start,
            timeRemaining: this.props.timer
        });

        this.timer = setInterval(() => this.tick(), 50);
    }

    stopHandler() {
        if (this.timer) {
            clearInterval(this.timer);
            this.props.onFinish(this.state.position);
        }
    }

    static SetPosition() {
        return Math.floor((Math.random() * Math.floor(4)));
    }

    changePosition() {
        this.setState({
            position: Spinner.SetPosition(),
            timeRemaining: this.state.timeRemaining + 100
        });
    }

    tick() {
        if (this.state.timeRemaining >= 9800) {
            clearInterval(this.timer);
            this.props.onFinish(this.state.position);

        } else {
            this.changePosition();
        }
    }

    render() {
        const { items } = this.props,
            { position } = this.state;

        return (
            <div style={ {
                display: 'block',
                border: '1px solid #000',
                padding: '40px 20px',
                minWidth: '100px',
                textAlign: 'center'
            } }>
                { items[position] }
            </div>
        );
    }
}

export default Spinner;
