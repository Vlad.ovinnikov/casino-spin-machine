/**
 * Created by: Vladyslav
 * Date: 19/01/2019
 * Time: 11:42
 */
import React, { PureComponent } from 'react';
import { Spinner } from '../../components';

class Main extends PureComponent {

    items = ['strawberry', 'banana', 'orange', 'a monkey'];
    spinners = [2200, 1400, 1000];
    matches = [];

    state = {
        winner: null,
        start: false
    };

    constructor(props) {
        super(props);

        this.finishHandler = this.finishHandler.bind(this);
        this.startHandle = this.startHandle.bind(this);
        this.stopHandle = this.stopHandle.bind(this);
        this.getInformed = this.getInformed.bind(this);
        this.renderSpinners = this.renderSpinners.bind(this);
    }

    componentDidMount() {
        this.setTimeout(this.startHandle, 5000);
    }

    componentWillUnmount() {
        clearTimeout(this.timer);
    }

    setTimeout(fn, timeout) {
        this.timer = setTimeout(() => {
            fn();
        }, timeout);
    }

    startHandle() {
        clearTimeout(this.timer);

        this.setState({ start: true });
        this.setTimeout(this.stopHandle, 5000);
    }

    stopHandle() {
        clearTimeout(this.timer);
        this.setState({ start: false });
    }

    finishHandler(value) {
        this.setState({ start: false });
        this.matches.push(value);

        if (this.matches.length === 3) {
            const winner = this.matches.reduce((acc, el, i, arr) => {

                const full = this.matches.filter(m => m === el);

                if (full.length === 3) {
                    acc.full = true;
                }

                if (arr.indexOf(el) !== i && acc.dupl.el !== el) {
                    acc.dupl.el = el;
                    acc.dupl.consecutive = arr[i - 1] === el;
                }

                return acc;
            }, { dupl: {}, full: false });

            this.setState({ winner });
            this.matches = [];
        }
    }

    getInformed() {
        const { winner } = this.state;

        if (!winner) {
            return <span>Check your luck!</span>;
        }
        if (winner.full) {
            return <span style={ { color: 'green' } }>You win $100!!!!</span>;

        } else if (typeof winner.dupl.el !== 'undefined') {
            const text = winner.dupl.consecutive ? 'You win $20!' : 'You win $10!';
            return <span style={ { color: 'orange' } }>{ text }</span>;
        }

        return <span style={ { color: 'red' } }>Hey, you lost!</span>;
    }

    renderSpinners() {
        return this.spinners.map((spinnerTime, key) => (
            <Spinner key={ key }
                     items={ this.items }
                     onFinish={ this.finishHandler }
                     isStart={ this.state.start }
                     timer={ spinnerTime }/>
        ));
    }

    render() {
        return (
            <div style={ { padding: '10px' } }>
                <h2 style={ { textAlign: 'center' } }>Casino spin machine</h2>
                <h4>{ this.getInformed() }</h4>
                <div style={ {
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    margin: '20px'
                } }>
                    <button style={ { padding: '10px 20px', marginRight: '20px' } }
                            aria-label='Start'
                            onClick={ this.startHandle }>
                        Start
                    </button>
                    <button style={ { padding: '10px 20px' } }
                            aria-label='Stop'
                            onClick={ this.stopHandle }>
                        Stop
                    </button>
                </div>
                <div style={ {
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    margin: '20px'
                } }>
                    { this.renderSpinners() }
                </div>

            </div>
        );
    }
}

export default Main;
