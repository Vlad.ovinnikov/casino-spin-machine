/**
 * Created by: Vladyslav
 * Date: 19/01/2019
 * Time: 11:42
 */
import React, { Fragment } from 'react';
import { Main } from './layouts';
import './styles/index.scss';

export default () => {
    return (
        <Fragment>
            { /* Main container */ }
            <Main/>
        </Fragment>
    );
};
